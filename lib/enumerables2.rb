require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.length == 0
  arr.inject(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |long_string|
    return false if !(substring?(long_string, substring))
  end
  true
end

def substring? long_string, substring
  substrings(long_string).include? substring
end

def substrings(string)
  substrings = []
  i = 0
  while i < string.length
    j = i + 1
    while j < string.length
      substrings << string[i..j]
      j += 1
    end
    i += 1
  end
  substrings += string.split ""
  substrings.uniq
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  once = []
  more = []
  words = string.split
  words.each do |word|
    word.each_char do |char|
      if once.include? char
        more << char
      else
        once << char
      end
    end
  end
  more.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split
  sorted = words.sort_by { |word| word.length }
  sorted[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  missing = []
  letters = ("a".."z").to_a
  letters.each do |letter|
    missing << letter if !string.include?(letter)
  end
  missing.uniq
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  no_repeats = []
  (first_yr..last_yr).to_a.each { |year| no_repeats << year if not_repeat_year?(year) }
  no_repeats
end

def not_repeat_year?(year)
  year_array = year.to_s.split ""
  year_array.uniq == year_array
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  repeats = []
  songs.each.with_index do |song, index|
    break if index == songs.length - 1
    if song == songs[index + 1]
      repeats << song
    end
  end
  songs.uniq - repeats
end

def no_repeats?(song_name, songs)

end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  last_c = ""
  words = string.split
  words.each do |word|
    word_clean = remove_punctuation(word)
    if c_distance(word_clean) != nil
      if last_c == ""
        last_c = word_clean
      elsif c_distance(word_clean) < c_distance(last_c)
        last_c = word_clean
      end
    end
  end
  last_c
end

def c_distance(word)
  return nil if word.rindex("c") == nil
  word.length - word.rindex("c")
end

def remove_punctuation word
  word.delete ".,?!"
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeats = []
  stack = []
  arr.each.with_index do |num, index|
    stack << index
    if num != arr[index + 1]
      if stack.length > 1
        repeats << [stack[0],stack[-1]]
      end
      stack = []
    end
  end
  repeats
end
